def deploy(projectName) {
    stage 'Update Rancher stack'
    env.PROJECT_NAME = projectName
    upgrade(projectName)
    try {
        stage 'Confirm Update'
        // timeout(time:60, unit:'SECONDS') {
        //     input message: "Does ${projectName} stack look good?"
        // }
        rancher.confirm()
    } catch (err) {
        stage 'Rolling back'
        // rancher.rollback()
    }
}

// Upgrades all services in the stack (i.e. docker-compose.yml)
def upgrade(projectName) {
    echo "${projectName} upgrade"
    sh "make upgrade"
}

// Force an upgrade even though the docker-compose.yml for the services didn't change
def forceUpgrade(projectName) {
    echo "${projectName} force upgrade"
    sh "make confirm-upgrade"
}

// Confirm that the upgrade is complete and successful
def confirm() {
    sh "make confirm-upgrade"
}

// Roll back to previous version
def rollback() {
    sh "make rollback"
}

// !!Important Boilerplate!!
// The external code must return it's contents as an object
return this;

def composerInstall() {
    stage 'Install vendors'
    workspace = pwd()
    sh "docker run -v ${workspace}:/app -v ${env.JENKINS_HOME}/.ssh:/root/.ssh composer/composer install --prefer-dist --ignore-platform-reqs --no-dev --no-progress"
    sh "docker run -v ${workspace}/vendor:/vendor ubuntu chmod -R a+rw /vendor"
    commitVendors()
}

def commitVendors() {
    stage 'Commiting vendors'
    sh "find vendor -type d -name .git -print0 | xargs -0 rm -rf"
    sh "find vendor -type f -name .gitignore -print0 | xargs -0 rm -rf"
    sh "sed -i -e '/^.*vendor/ d' .gitignore"
    try {
        sh "git add -f ."
        sh "git commit -m 'Vendors build ${env.BUILD_NUMBER}'"
    } catch (err) {
        echo "Nothing new to commit"
    }
}

return this;


// scmOrigin = "git@bitbucket.org:weebonline/service.library.git"
// scmRemote = "ssh://root@git.library.development.weeb.online:2222/app.git"
// branch = 'refs/remotes/origin/master'

def fetchScm(branch, scmOrigin, host, port) {
    stage 'Fetch remotes'
    scmRemote = "ssh://root@${host}:${port}/app.git"
    try {
        checkoutScm(branch, scmOrigin, scmRemote)
    } catch (err) {
        sh "sshpass -p root ssh-copy-id root@${host} -p ${port}"
        checkoutScm(branch, scmOrigin, scmRemote)
    }
}

def checkoutScm(branch, scmOrigin, scmRemote) {
    checkout([
        $class: 'GitSCM',
        branches: [[name: branch]],
        doGenerateSubmoduleConfigurations: false,
        userRemoteConfigs: [
            [name: 'origin', url: scmOrigin],
            [name: 'remote', url: scmRemote]
        ]
    ])
}

def pushRemote(tag) {
    sh "git tag -f ${tag}"
    sh "git push remote ${tag}"
}

return this;

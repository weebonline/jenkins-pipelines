
// Loads config provided by 'Config File Provider Plugin'
// https://wiki.jenkins-ci.org/display/JENKINS/Config+File+Provider+Plugin
def loadProvidedConfig(fileId) {
    stage 'Configuration'
    wrap([$class: 'ConfigFileBuildWrapper', managedFiles: [[fileId: fileId, replaceTokens: false, targetLocation: '', variable: 'config']]]) {
        load env.config
    }
}
return this;

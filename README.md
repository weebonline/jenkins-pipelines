# Init

```
git checkout --orphan rancher
git remote add pipelines git@bitbucket.org:weebonline/jenkins-pipelines.git
git fetch pipelines && git merge pipelines/rancher
```

To get basic config
```
git remote add media git@bitbucket.org:weebonline/media.git
git fetch media && git cherry-pick media/rancher dbe75264c21fef1287728a0a6b8824a5ad5389e7
```

git reset --hard && git cherry-pick --continue  
git push
